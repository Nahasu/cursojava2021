package com.gabrielCode.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.gabrielCode.service.IPersonaService;

@Service

public class PersonaRepoImpl1 implements IPersonaService{

	@Autowired
	@Qualifier("persona1")
	IPersona repo;
	
	@Override
	public void registrarHandler(String pNombre) {
		
		repo.registrar(pNombre);
	}

}
