package com.gabrielCode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Usuario {


		@Id
		private int codigo;
		@Column(name = "NOMBRE", length = 50)
		private String nombre;
		@Column(name = "CLAVE", length = 50)
		private String clave;
		
		public Usuario(int pCod, String pNombre, String pClave) {
			
			codigo = pCod;
			clave = pClave;
			nombre = pNombre;
		}
		
		public Usuario() {
			
		}
		
		
		public int getCodigo() {
			return codigo;
		}
		public void setCodigo(int codigo) {
			this.codigo = codigo;
		}
		public String getClave() {
			return clave;
		}
		public void setClave(String clave) {
			this.clave = clave;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}		
}

