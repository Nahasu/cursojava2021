package modulo2;

public class Mod2_Ejercicio2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		byte bmin = -128;
		byte bmax = 127;
		short smin = -32768;
		short smax = 32767;
		int imin = -(int)(Math.pow(2, 31));
		int imax = (int)(Math.pow(2, 31)-1);
		long lmin = -9223372036854775807l;
		long lmax = 9223372036854775807l;
		
		System.out.println("Tipo de dato\tMinimo\t\t\t\tMaximo");
		System.out.println("\n------------\t-------\t\t\t\t-------");
		System.out.println("\nbyte\t\t" + bmin + "\t\t\t\t" + bmax);
		System.out.println("short\t\t" + smin + "\t\t\t\t" + smax);
		System.out.println("int\t\t" + imin + "\t\t\t" + imax);
		System.out.println("long\t\t" + lmin + "\t\t" + lmax);

	}

}
