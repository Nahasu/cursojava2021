package patrones.factory;

import java.util.ArrayList;
import java.util.List;

public abstract class PiedraPapelTijeraFactory {

	private static List<PiedraPapelTijeraFactory>elementos;
	protected String nombre;
	protected int numero;
	protected String descripcionResultado;
	
	public PiedraPapelTijeraFactory() {
		
		
	}

	//ACCESOS
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	
	
	public String getDescripcionResultado() {
		return descripcionResultado;
	}

	public static PiedraPapelTijeraFactory getInstance(int num) {
		
		//el padre conoce todos los hijos
		
		elementos = new ArrayList<PiedraPapelTijeraFactory>();
		elementos.add(new Piedra());
		elementos.add(new Papel());
		elementos.add(new Tijera());
		
		for(PiedraPapelTijeraFactory piedraPapelTijeraFactory : elementos) {
			
			if(piedraPapelTijeraFactory.isMe(num)) {
				
				return piedraPapelTijeraFactory;
			}		
		}
		return null;
	}
	
	public abstract boolean isMe(int num);
	
	public abstract int comparar(PiedraPapelTijeraFactory pptf);
}
