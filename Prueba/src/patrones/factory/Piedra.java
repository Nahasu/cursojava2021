package patrones.factory;

public class Piedra extends PiedraPapelTijeraFactory {

	public Piedra() {
		
		numero = 0;
		nombre = "piedra";
	}

	@Override
	public boolean isMe(int num) {
		
		return num==0;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pptf) {
		
		//con piedra (0) empata------resultado 0
		//con papel (1) pierdo-------resultado -1
		//con tijera (2) gano--------resultado 1
		
		int resultado = 0;
		
		switch(pptf.getNumero()) {
		
			case 0:
			
				resultado = 0;
				descripcionResultado = "empate";
				break;
				
			case 1:
				
				resultado = -1;
				descripcionResultado = "piedra perdio contra " + pptf.getNombre();
				break;
			
			case 2:
				
				resultado = 1;
				descripcionResultado = "piedra gano a " + pptf.getNombre();
				break;
		}
		
		
		return resultado;
	}

}
