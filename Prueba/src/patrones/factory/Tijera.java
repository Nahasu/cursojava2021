package patrones.factory;

public class Tijera extends PiedraPapelTijeraFactory {

	public Tijera() {
		
		numero = 2;
		nombre = "tijera";
	}

	@Override
	public boolean isMe(int num) {
		
		return num==2;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pptf) {
		
		//con piedra (0) pierdo------resultado -1
		//con papel (1) gano-------resultado 1
		//con tijera (2) empato--------resultado 0
				
			int resultado = 0;
				
			switch(pptf.getNumero()) {
				
				case 0:
					
					resultado = -1;
					break;
						
				case 1:
						
					resultado = 1;
					break;
					
				case 2:
						
					resultado = 0;
					break;
			}
					
			return resultado;
	}
}
