package patrones.factory;

public class Papel extends PiedraPapelTijeraFactory {

	public Papel() {
		
		numero = 1;
		nombre = "papel";
	}

	@Override
	public boolean isMe(int num) {
		
		return num==1;
	}

	@Override
	public int comparar(PiedraPapelTijeraFactory pptf) {
		
		//con piedra (0) gano------resultado 1
		//con papel (1) empato-------resultado 0
		//con tijera (2) pierdo--------resultado -1
		
		int resultado = 0;
		
		switch(pptf.getNumero()) {
		
			case 0:
			
				resultado = 1;
				break;
				
			case 1:
				
				resultado = 0;
				break;
			
			case 2:
				
				resultado = -1;
				break;
		}
		
		
		return resultado;
	}

}
