package Modulo5_Strings;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		boolean hay = false;
		
		System.out.println("Introduce el texto");
		char[] chars = read.nextLine().toCharArray();
		
		for(int i = 0; i < chars.length; i++) {
			
			if(Character.isDigit(chars[i])) {
				
				hay = true;
			}
		}
		
		if(hay == true) {
			
			System.out.println("Hay numeros");
		}
		
	}

}
