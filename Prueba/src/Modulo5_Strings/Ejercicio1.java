package Modulo5_Strings;

import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Escribe una palabra o frase");
		String texto = read.nextLine();
		
		System.out.println(texto.toUpperCase());
		System.out.println(texto.toLowerCase());
		System.out.println(texto.toLowerCase().replace('o', '2'));
	}

}
