package Modulo5_Strings;

public class Ejercicio5 {

	public static void main(String[] args) {
		
		String mail = "gcasas1972@gmail.com";
		int pos = 0;
		
		for(int i = 0; i < mail.length(); i++) {
			
			if(mail.charAt(i) == '@') {
				
				pos = i;
			}
		}
		
		String[] partes = mail.split("@");
		
		System.out.println("La posicion de la @ es " + (pos+1));
		System.out.println("La primera parte es " + partes[0]);
		System.out.println("La segunda parte es " + partes[1]);
	}

}
