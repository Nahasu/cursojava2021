package Modulo5_Strings;

public class Ejercicio4 {

	public static void main(String[] args) {
		
		String texto = "esto es una prueba de la clase String";
		int vocales = 0;
		int consonantes = 0;
		
		texto = texto.toLowerCase();
		
		for(int i =0; i < texto.length(); i++) {
			
			if(texto.charAt(i) == 'a' || texto.charAt(i) == 'e' || texto.charAt(i) == 'i' || texto.charAt(i) == 'o' || texto.charAt(i) == 'u') {
				
				vocales++;
				
			}else if(texto.charAt(i) == ' ') {
				
				
			}else {
				
				consonantes++;
			}
		}
		
		System.out.println("Hay " + vocales + " vocales");
		System.out.println("Hay " + consonantes + " consonantes");
	}
}
