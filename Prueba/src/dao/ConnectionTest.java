package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionTest {
	
	public ConnectionTest() {
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try {
			//1-activar el driver
			
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//2-conectar 
			
			Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/Manpower", "sistema", "sistema");
			
			//3-crear la sentencia
			
			Statement stm = conexion.createStatement();
			
			//4-ejecutar la consulta
			
			ResultSet rs = stm.executeQuery("select alu_id, alu_apellido, alu_nombre, alu_estudios, alu_linkgit from alumnos");
			
			while(rs.next()) {
				
				System.out.println("apellido=" + rs.getString("alu_apellido"));
				System.out.println(", nombre=" + rs.getString("alu_nombre"));
			}
			
			rs.close();
			conexion.close();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
