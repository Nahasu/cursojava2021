package modulo7.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modulo7.Profesor;

public class ProfesorTest {
	
	List <Profesor> profesorLista;
	Set <Profesor> profesorSet;
	Profesor profesorEjemplo;
	
	
	@Before
	public void setUp() throws Exception {
		profesorEjemplo = new Profesor("Guille","Jimenez","258");
		
		profesorLista = new ArrayList<Profesor>();
		profesorLista.add(new Profesor("Guille","Jimenez","258"));
		profesorLista.add(new Profesor("Sebastian","Rioja","666"));
		profesorLista.add(new Profesor("Alberto","Sanchez","875"));
		
		profesorSet = new HashSet<Profesor>();
		profesorSet.add(new Profesor("Guille","Jimenez","258"));
		profesorSet.add(new Profesor("Sebastian","Rioja","666"));
		profesorSet.add(new Profesor("Alberto","Sanchez","875"));
	}

	@After
	public void tearDown() throws Exception {
		profesorLista = null;
		profesorSet = null;
		profesorEjemplo = null;
	}

	@Test
	public void testHashCodeProfesor() {
		assertTrue(profesorSet.contains(profesorEjemplo));
	}

	@Test
	public void testEqualsProfesor() { //
		assertTrue(profesorLista.contains(profesorEjemplo));
		Profesor prueba = new Profesor("Pepe","Larraz","2367");
		assertFalse(profesorEjemplo.equals(prueba)); 
	}

	@Test
	public void testProfesorContructor() {
		assertEquals("Guille",profesorEjemplo.getNombre()); 
		profesorEjemplo.setIosfa("258"); 
		assertEquals("258",profesorEjemplo.getIosfa());
	}
	
}
