package es.com.manpower.notas.util;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DateUtilTest {

	Date fecha;
	
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		
		Calendar cal = Calendar.getInstance();
		cal.set(1972, Calendar.NOVEMBER, 17);
		fecha = cal.getTime();
	}

	@AfterEach
	void tearDown() throws Exception {
		fecha = null;
	}

	@Test
	void testGetAnio() {
		assertEquals(1972, DateUtil.getAnio(fecha));
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		System.out.println("fecha " + fecha);
		System.out.println("calendar " + cal);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd-EEE");
		System.out.println("fecha formateada " + sdf.format(fecha));
	}

}
