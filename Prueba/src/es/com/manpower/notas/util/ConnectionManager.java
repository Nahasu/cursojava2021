package es.com.manpower.notas.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	private static Connection conexion;
	
	public static void conectar() throws ClassNotFoundException, SQLException{
		
		//1-activar el driver
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		
		//2-conectar 
		
		conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/Manpower", "sistema", "sistema");
	}
	
	
	public static void desConectar() throws SQLException {
		
		conexion.close();
	}
	
	public static Connection getConexion() {
		
		return conexion;
	}
	
}
