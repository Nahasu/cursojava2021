package es.com.manpower.notas.util;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConnectionManagerTest {

	Connection con;
	
	@BeforeEach
	void setUp() throws ClassNotFoundException, SQLException {
		
		Class.forName("com.mysql.cj.jdbc.Driver");
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/Manpower", "sistema", "sistema");
		
		ConnectionManager.conectar();
	}

	@AfterEach
	void tearDown() throws SQLException {
		
		con = null;
		
		ConnectionManager.desConectar();
	}

	@Test
	void testConectar() {
		try {
			ConnectionManager.conectar();
			assertFalse(ConnectionManager.getConexion().isClosed());
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	void testDesConectar() {
		try {
			ConnectionManager.desConectar();
			assertTrue(ConnectionManager.getConexion().isClosed());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	void testGetConexion() throws SQLException {
		assertFalse(ConnectionManager.getConexion().isClosed());
	}

}
