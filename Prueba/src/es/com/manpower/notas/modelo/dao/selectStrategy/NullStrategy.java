package es.com.manpower.notas.modelo.dao.selectStrategy;

public class NullStrategy extends SelectStrategy {

	
	
	public NullStrategy() {
		
		this.isUltimo = false;
		this.tengoWhere = false;
	}

	@Override
	public String getCondicion() {
		
		return "";
	}

	@Override
	public boolean isMe() {
		
		this.isUltimo = this.alum == null;
		return this.alum == null;
	}

}
