package es.com.manpower.notas.modelo.dao.selectStrategy;

import java.util.ArrayList;
import java.util.List;

import es.com.manpower.notas.modelo.Alumno;

public abstract class SelectStrategy {

	protected boolean isUltimo;
	protected static StringBuilder sql; 
	protected static boolean tengoWhere;
	protected static Alumno alum;
	
	public SelectStrategy() {
		
	}
	
	public static String getSql(Alumno alumno) {
		//conoce todos los hijos, voy a armar la sentencia seg�n lo que hayan determinado las clases
		
		sql = new StringBuilder("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos");
		
		alum = alumno;
		
		List<SelectStrategy> estrategias = new ArrayList<SelectStrategy>();
		estrategias.add(new NullStrategy());
		estrategias.add(new VacioStrategy());
		estrategias.add(new Codigo0Strategy());
		estrategias.add(new NombreStrategy());
		estrategias.add(new ApellidoStrategy());
		estrategias.add(new EstudiosStrategy());
		estrategias.add(new LinkStrategy());
		
		for(SelectStrategy selectStrategy : estrategias) {
			
			if(selectStrategy.isMe()) 
				sql.append(selectStrategy.getCondicion());
			if(selectStrategy.isUltimo)
				return sql.toString();
		}
		
		return sql.toString();
	}
	
	public abstract String getCondicion();
	public abstract boolean isMe();
}
