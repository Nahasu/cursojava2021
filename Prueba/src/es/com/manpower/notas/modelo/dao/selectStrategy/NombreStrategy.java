package es.com.manpower.notas.modelo.dao.selectStrategy;

public class NombreStrategy extends SelectStrategy {

	public NombreStrategy() {
		
	}

	@Override
	public String getCondicion() {
		
		StringBuilder sb = new StringBuilder(" where alu_nombre like '%");
		sb.append(alum.getNombre());
		sb.append("%'");
		
		return sb.toString();
	}

	@Override
	public boolean isMe() {	
		
		tengoWhere =alum.getNombre()!= null && !alum.getNombre().isEmpty();
		
		return !alum.getNombre().isEmpty() && !alum.getNombre().isBlank();
	}
}
