package es.com.manpower.notas.modelo.dao.selectStrategy;

public class LinkStrategy extends SelectStrategy {

	public LinkStrategy() {
	
	}

	@Override
	public String getCondicion() {
		
		StringBuilder sb = new StringBuilder();
		
		if(tengoWhere) {
			sb.append(" and alu_linkgit like '%");
			sb.append(alum.getLinkRepositorio());
			sb.append("%'");
		}else {
			
			sb.append("where alu_linkgit like '%");
			sb.append(alum.getLinkRepositorio());
			sb.append("%'");
			tengoWhere = true;
		}
		
		return sb.toString();
	}

	@Override
	public boolean isMe() {
		
		return alum.getLinkRepositorio()!= null && !alum.getLinkRepositorio().isEmpty();
	}

}
