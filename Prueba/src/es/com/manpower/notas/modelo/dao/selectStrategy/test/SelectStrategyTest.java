package es.com.manpower.notas.modelo.dao.selectStrategy.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.com.manpower.notas.modelo.Alumno;
import es.com.manpower.notas.modelo.dao.selectStrategy.SelectStrategy;

class SelectStrategyTest {

	Alumno aluVacio;
	Alumno aluConCodigo;
	Alumno aluConNombre;
	Alumno aluConNombreYApellido;
	Alumno aluConEstudios;
	Alumno aluConNombreEstudiosLink;;
	
	
	@BeforeEach
	void setUp() throws Exception {
		
		aluVacio = new Alumno();
		aluConCodigo = new Alumno(10);
		aluConNombre = new Alumno(0, "Gabriel", null, null, null);
		aluConNombreYApellido = new Alumno(0, "Gabriel", "Casas", null, null);
		aluConEstudios = new Alumno(0, null, null, "DAM", null);
		aluConNombreEstudiosLink = new Alumno(0, "Gabriel", null, "DAM","http://gitlab/gcasas1972");
	}

	@AfterEach
	void tearDown() throws Exception {
		aluConCodigo = null;
		aluConNombre = null;
		aluConEstudios = null;
		aluVacio = null;
		aluConNombreEstudiosLink = null;
		aluConNombreYApellido = null;
	}

	
	@Test
	public void testGetSqlVacio() {
		
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos", SelectStrategy.getSql(aluVacio));
	}
	
	@Test
	public void testGetSqlNull() {
		
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos", SelectStrategy.getSql(null));
	}
	
	@Test
	void testGetSqlCodigo() {
		
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_id =10", SelectStrategy.getSql(aluConCodigo));
	}
	
	@Test
	void testGetSqlNombre() {
		
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_nombre like '%Gabriel%'", SelectStrategy.getSql(aluConNombre));
	}

	@Test
	void testGetSqlNombreYApellido() {
		
		StringBuilder sb = new StringBuilder("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit ");
		sb.append("from alumnos ");
		sb.append("where alu_nombre like '%Gabriel%' ");
		sb.append("and alu_apellido like '%Casas%'");
		
		assertEquals(sb.toString(), SelectStrategy.getSql(aluConNombreYApellido));
	}
	
	void testGetSqlEstudios() {
		
		assertEquals("select alu_id, alu_nombre, alu_apellido, alu_estudios, alu_linkgit from alumnos where alu_estudios like '%DAM%'", SelectStrategy.getSql(aluConEstudios));
	}
}
