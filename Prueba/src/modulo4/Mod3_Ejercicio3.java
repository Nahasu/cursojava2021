package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio3 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el mes en minusculas");
		String mes = read.nextLine();
		
		if(mes.equals("enero") || mes.equals("marzo") || mes.equals("mayo") || mes.equals("julio") || mes.equals("agosto") || mes.equals("octubre") || mes.equals("diciembre")) {
			
			System.out.println("Tiene 31 dias");
			
		}else if(mes.equals("abril") || mes.equals("junio") || mes.equals("septiembre") || mes.equals("noviembre")) {
			
			System.out.println("Tiene 30 dias");
		}else {
			
			System.out.println("Tiene 28 dias");
		}

	}

}
