package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio16 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el numero");
		int num = read.nextInt();
		
		for(int i = 1; i<=10; i++) {
			
			System.out.println(num + "*" + i + "=" + num * i);
		}

	}

}
