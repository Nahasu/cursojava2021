package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio12 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce un n�mero");
		int num = read.nextInt();
		
		if((num >= 1) && (num <= 12)) {
			
			System.out.println("El numero pertenece a la primera docena");
		}else if((num >= 13) && (num <= 24)) {
			
			System.out.println("El numero pertenece a la segunda docena");
		}else if((num >= 25) && (num <= 36)) {
			
			System.out.println("El numero pertenece a la tercera decena");
		}else {
			
			System.out.println("El numero " + num + " esta fuera de rango");
		}

	}

}
