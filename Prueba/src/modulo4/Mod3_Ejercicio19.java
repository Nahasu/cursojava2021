package modulo4;

public class Mod3_Ejercicio19 {

	public static void main(String[] args) {
		
		int contador = 1;
		int suma = 0;
		int num = 0;
		
		while(contador <= 10) {
			
			num = (int)(Math.random()*100);
			System.out.println(num);
			suma += num;
			
			contador++;
		}
		
		System.out.println("La suma total es " + suma);
		System.out.println("El promedio es " + (suma/contador));

	}

}
