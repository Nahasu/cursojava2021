package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio11 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el caracter");
		char car = read.next().charAt(0);
		
		if((car == 'a') || (car == 'e') || (car == 'i') || (car == 'o') || (car == 'u')) {
			
			System.out.println("Es una vocal");
		}else {
			
			System.out.println("Es una consonante");
		}

	}

}
