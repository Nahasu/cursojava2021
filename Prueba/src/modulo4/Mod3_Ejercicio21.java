package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio21 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce la categor�a: A, B o C");
		char cat = read.next().charAt(0);
		read.nextLine(); //PARA LIMPIAR EL CACH� TRAS LEER STRINGS, LO HAGO SIEMPRE POR SI ACASO PORQUE ME HA DADO SUSTOS ANTES
		
		System.out.println("Introduce la antiguedad");
		int ant = read.nextInt();
		
		System.out.println("Introduce el sueldo");
		int sueldo = read.nextInt();
		
		if((ant >= 1) && (ant <=5)) {
			
			if((cat == 'A') || (cat == 'a')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/20))+1000);
				
			}else if((cat == 'B') || (cat == 'b')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/20))+2000);
				
			}else if((cat == 'C') || (cat == 'c')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/20))+3000);
				
			}
		}else if((ant >= 6) && (ant <=10)) {
			
			if((cat == 'A') || (cat == 'a')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/10))+1000);
				
			}else if((cat == 'B') || (cat == 'b')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/10))+2000);
				
			}else if((cat == 'C') || (cat == 'c')) {
				
				System.out.println("El sueldo neto es " + (sueldo + (sueldo/10))+3000);
				
			}
			
		}else if(ant > 10){
			
			if((cat == 'A') || (cat == 'a')) {
				
				System.out.println("El sueldo neto es " + ((sueldo + (sueldo/3.33))+1000));
				
			}else if((cat == 'B') || (cat == 'b')) {
				
				System.out.println("El sueldo neto es " + ((sueldo + (sueldo/3.33))+2000));
				
			}else if((cat == 'C') || (cat == 'c')) {
				
				System.out.println("El sueldo neto es " + ((sueldo + (sueldo/3.33))+3000));	
			}
		}
	}
}
