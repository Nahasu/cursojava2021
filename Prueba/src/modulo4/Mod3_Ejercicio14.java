package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio14 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el puesto");
		int puesto = read.nextInt();
		
		switch(puesto) {
		
			case 1:
				
				System.out.println("Medalla de oro");
				break;
				
			case 2:
				
				System.out.println("Medalla de plata");
				break;
				
			case 3:
				
				System.out.println("Medalla de bronce");
				break;
				
			default:
				
				System.out.println("Sigue participando");
				break;
		}

	}

}
