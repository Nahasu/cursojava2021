package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio13 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el mes en minusculas");
		String mes = read.nextLine();
		
		switch(mes) {
		
			case "enero":
			case "marzo":
			case "mayo":
			case "julio":
			case "agosto":
			case "octubre":
			case "diciembre":
				
				System.out.println("31 d�as");
				break;
			
			case "abril":
			case "junio":
			case "septiembre":
			case "noviembre":
				
				System.out.println("30 d�as");
				break;
				
			case "febrero":
				
				System.out.println("28 d�as");
				break;
 		}

	}

}
