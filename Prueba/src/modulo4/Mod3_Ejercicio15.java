package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio15 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el caracter para mostrar las caracteristicas");
		char car = read.next().charAt(0);
		
		switch(car) {
		
			case 'a':
			
				System.out.println("4 ruedas y un motor");
				break;
			
			case 'b':
				
				System.out.println("4 ruedas, un motor, cierre centralizado y aire");
				break;
				
			case 'c':
				
				System.out.println("4 ruedas, un motor, cierre centralizado, aire y airbag");
				break;
				
			default:
			
				System.out.println("Input erroneo");
				break;
		}

	}

}
