package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio17 {

	public static void main(String[] args) {
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el numero");
		int num = read.nextInt();
		int suma = 0;
		
		for(int i = 1; i<=10; i++) {
			
			if((num*i)%2 == 0) {
				suma += (num*i);
			}
		}
		
		System.out.println("La suma es " + suma);
	}

}
