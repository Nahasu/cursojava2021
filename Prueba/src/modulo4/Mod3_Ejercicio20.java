package modulo4;

public class Mod3_Ejercicio20 {

	public static void main(String[] args) {
		
		int contador = 1;
		int max = 0;
		int min = 99;
		int num = 0;
		
		while(contador <= 10) {
			
			num = (int)(Math.random()*100);
			System.out.println(num);
			
			if(num >= max) {
				max = num;
			}
			if(num <= min) {
				min = num;
			}
			
			contador++;
		}
		
		System.out.println("El mayor es " + max);
		System.out.println("El menor es " + min);

	}

}
