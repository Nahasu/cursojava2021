package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio10 {

	public static void main(String[] args) {
		
		int a = 0;
		int b = 0;
		int c = 0;
		
		Scanner read = new Scanner (System.in);
		
		System.out.println("Introduce el primer valor");
		a = read.nextInt();
		
		System.out.println("Introduce el segundo valor");
		b = read.nextInt();
		
		System.out.println("Introduce el tercer valor");
		c = read.nextInt();
		
		if((a >= b) && (a >= c)) {
			
			System.out.println("El valor superior es " + a);
		}else if((b >= a) && (b >= c)) {
			
			System.out.println("El valor superior es " + b);
		}else {
			
			System.out.println("El valor superior es " + c);
		}

	}

}
