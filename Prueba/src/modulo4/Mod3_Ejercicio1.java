package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Introduce la primera nota");
		Scanner read = new Scanner(System.in);
		float nota1 = read.nextFloat();
		
		System.out.println("Introduce la segunda nota");
		float nota2 = read.nextFloat();
		
		System.out.println("Introduce la tercera nota");
		float nota3 = read.nextFloat();
		
		float promedio = (nota1 + nota2+ nota3)/3;
		
		if(promedio>=7) {
			
			System.out.println("Has aprobado");
		}else {
			
			System.out.println("Mal");
		}
		
		System.out.println("El promedio es " + promedio);
	}
	
	

}
