package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner read = new Scanner(System.in);
		
		System.out.println("Introduce el puesto");
		int puesto = read.nextInt();
		
		if(puesto == 1) {
			
			System.out.println("Obtiene la medalla de oro");
		}else if(puesto == 2) {
			
			System.out.println("Obtiene la medalla de plata");
		}else if(puesto == 3) {
			
			System.out.println("Obtiene la medalla de bronce");
		}else {
			
			System.out.println("Sigue participando");
		}
	}

}
