package modulo4;

import java.util.Scanner;

public class Mod3_Ejercicio9 {

	public static void main(String[] args) {
		
		int jug1 = 0;
		int jug2 = 0;

		Scanner read = new Scanner(System.in);

		System.out.println("Jugador 1, elige piedra, papel o tijera");
		jug1 = read.nextInt();
		System.out.println("Jugador 2, elige piedra, papel o tijera");
		jug2 = read.nextInt();

		if (((jug1 == 0) && (jug2 == 2)) || ((jug1 == 1) && (jug2 == 0)) || ((jug1 == 2) && (jug2 == 1))) {

			System.out.println("Jugador 1 gana");
		} else if (((jug2 == 0) && (jug1 == 2)) || ((jug2 == 1) && (jug1 == 0)) || ((jug2 == 2) && (jug1 == 1))) {

			System.out.println("Jugador 2 gana");
		}else {
			
			System.out.println("Empate");
		}

	}

}
