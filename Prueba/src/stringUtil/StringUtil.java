package stringUtil;

public class StringUtil {

	
	
	public static boolean containsDobleSpace(String str) {
		
		int contador = 0;
		
		for(int i = 0; i < str.length(); i++) {
			
			if(str.charAt(i) == ' ') {
				
				contador++;
			}
		}
		
		if(contador > 2) {
			
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean containsNumber(String str) {
		
		boolean num = false;
		char[] chars = str.toCharArray();
		
		for(int i = 0; i < chars.length; i++) {
				
			if(Character.isDigit(chars[i])) {
				
				num = true;
			}
		}
		
		return num;
	}
	
}
