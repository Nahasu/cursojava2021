package stringUtil;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	static Calendar calendario;
	
	public static int getAnio(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		
		return cal.get(Calendar.YEAR);
	}
	
	public static int getMes(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		return cal.get(Calendar.MONTH);
	}
	
	public static int getDia(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		return cal.get(Calendar.DATE);
	}
	
	public static boolean isFinDeSemana(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		DayOfWeek day = DayOfWeek.of(cal.DAY_OF_WEEK);
		
		return day == DayOfWeek.SUNDAY || day == DayOfWeek.SATURDAY;
	}
	
	public static boolean isDiaDeSemana(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		DayOfWeek day = DayOfWeek.of(cal.DAY_OF_WEEK);
		
		return day != DayOfWeek.SUNDAY && day != DayOfWeek.SATURDAY;
	}
	
	public static int getDiaDeSemana(Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		
		return cal.DAY_OF_WEEK;
	}
	
	public static Date asDate(String pattern, String Fecha) {

		SimpleDateFormat formato = new SimpleDateFormat(pattern);
		Date fechaDate = null;

		try {
			fechaDate = formato.parse(Fecha);
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return fechaDate;
	}
	
	public static Calendar asCalendar(String pattern, String Fecha) {

		SimpleDateFormat formato = new SimpleDateFormat(pattern);
		Date fechaDate;

		try {
			fechaDate = formato.parse(Fecha);
			Calendar cal = Calendar.getInstance();
			cal.setTime(fechaDate);
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return calendario;
	}
	
	public static String asString(String pattern, Date Fecha) {

		SimpleDateFormat formato = new SimpleDateFormat(pattern);
		String resultado = " ";

		try {
			resultado = formato.format(Fecha);
		} catch (Exception ex) {
			System.out.println(ex);
		}

		return resultado;

	}
}
